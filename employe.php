
    <?php include 'nav.php'; ?>
    
    <?php

        try {
            $request = $pdo->prepare("SELECT * FROM employe ORDER BY date_embauche DESC");
            $request->execute();
            $request = $request->fetchAll();

        }
        catch (PDOException $e) {
            echo 'Error: '.$e->getMessage();
        }

    ?>

    <div class="container-fluid">
        <div class="row m-5">
            <div class="col-4 bg-dark text-white px-4 py-3">
                <h2>Liste du personnel</h2>
            </div>
            <div class="col-1 py-2 px-5 text-center">
                <a href="ajout-employe.php" class="ajouter text-decoration-none">
                    <div class="bg-dark text-danger" title="Ajouter">+</div>
                </a>
            </div>
        </div>
    </div>

    <div class="container text-center">

        <div class="row font-weight-bold border-bottom">
            <div class="col-1">
                <p>Nom</p>
            </div>
            <div class="col-1">
                <p>Prénom</p>
            </div>
            <div class="col-3">
                <p>Adresse</p>
            </div>
            <div class="col-1">
                <p>Code postal</p>
            </div>
            <div class="col-3">
                <p>Ville</p>
            </div>
            <div class="col-2">
                <p>Téléphone</p>
            </div>
            <div class="col-1">
                <p>Date d'embauche</p>
            </div>
        </div>

        <?php
            foreach ($request as $employe) {
            
            echo '  <a href="info-employe.php?id='.$employe['id_employe'].'" class="text-decoration-none text-black" title="infos">
                        <div class="row border-bottom text-decoration-none">
            
                            <div class="col-1 border-bottom mt-2">
                                <p class="text-dark">'.$employe['nom'].'</p>
                            </div>
                            <div class="col-1 border-bottom mt-2">
                                <p class="text-dark">'.$employe['prenom'].'</p>
                            </div>
                            <div class="col-3 border-bottom mt-2">
                                <p class="text-dark">'.$employe['adresse'].'</p>
                            </div>
                            <div class="col-1 border-bottom mt-2">
                                <p class="text-dark">'.$employe['code_postal'].'</p>
                            </div>
                            <div class="col-3 border-bottom mt-2">
                                <p class="text-dark">'.$employe['ville'].'</p>
                            </div>
                            <div class="col-2 border-bottom mt-2">
                                <p class="text-dark">'.$employe['telephone'].'</p>
                            </div>
                            <div class="col-1 border-bottom mt-2">
                                <p class="text-dark">'.$employe['date_embauche'].'</p>
                            </div>
                        </div>
                    </a>';
            }
        ?>

    </div>

