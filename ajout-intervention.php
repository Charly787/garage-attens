
    <?php include 'nav.php'; ?>

    <?php

        $intitule   = isset($_POST['intitule'])   && !empty($_POST['intitule'])   ? $_POST['intitule']   : '';
        $descrip    = isset($_POST['descrip'])    && !empty($_POST['descrip'])    ? $_POST['descrip']    : '';
        $date       = isset($_POST['date'])       && !empty($_POST['date'])       ? $_POST['date']       : '';
        $heure      = isset($_POST['heure'])      && !empty($_POST['heure'])      ? $_POST['heure']      : '';
        $duree      = isset($_POST['duree'])      && !empty($_POST['duree'])      ? $_POST['duree']      : '';
        $client     = isset($_POST['client'])     && !empty($_POST['client'])     ? $_POST['client']     : '';
        $mecanicien = isset($_POST['mecanicien']) && !empty($_POST['mecanicien']) ? $_POST['mecanicien'] : '';
        $submit     = isset($_POST['submit'])     && !empty($_POST['submit'])     ? $_POST['submit']     : '';

        if ($submit) {
            try {
                $request = $pdo->prepare("INSERT INTO intervention
                    (intitule,descrip,date,heure,duree,client,mecanicien)
                    VALUES (:intitule,:descrip,:date,:heure,:duree,:client,:mecanicien)");
                $request->execute([
                    'intitule'=>$intitule,'descrip'=>$descrip,'date'=>$date,'heure'=>$heure,'duree'=>$duree,'client'=>$client,
                    'mecanicien'=>$mecanicien]);

                    header('Location: intervention.php');

            }
            catch (PDOException $e) {
                echo 'Error: '.$e->getMessage();
            }
        }
    ?>

<div class="container-fluid text-center">
        <div class="row m-5">
            <div class="bg-dark text-white px-4 py-3">
                <h2>Nouvelle intervention</h2>
            </div>
            <div class="mt-3 px-4 ml-3 px-4 py-2">
                <a href="intervention.php" class="btn btn-dark text-white p-2">Retour</a>
            </div>
        </div>
    </div>

    <div class="container">
        <form method="post">
            <div class="row mt-5 font-weight-bold">
                <div class="col-4">
                    <p>Intitulé</p>
                </div>
                <div class="col-8">
                    <input type="text" name="intitule">
                </div>
                <div class="col-4">
                    <p>Description</p>
                </div>
                <div class="col-8">
                    <textarea type="text" name="descrip" cols="53" rows="3"></textarea>
                </div>
                <div class="col-4">
                    <p>Date</p>
                </div>
                <div class="col-8">
                    <input type="date" name="date">
                </div>
                <div class="col-4">
                    <p>Heure</p>
                </div>
                <div class="col-8">
                    <input type="time" name="heure">
                </div>
                <div class="col-4">
                    <p>Durée (mn)</p>
                </div>
                <div class="col-8">
                    <input type="text" name="duree">
                </div>
                <div class="col-4">
                    <p>Client</p>
                </div>
                <div class="col-8">
                    <input type="text" name="client">
                </div>
                <div class="col-4 border-bottom border-danger">
                    <p>Choisir un employé</p>
                </div>
                <div class="col-2 bloc">
                    <select name="mecanicien">
                        <option value="vide"></option>
                        <?php

                            try {
                                $mecan = $pdo->prepare("SELECT prenom FROM employe");
                                $mecan->execute();
                                
                                while ($meca = $mecan->fetch()) {

                                  echo '<option>' . $meca['prenom'] . '</option> ';
                                }
                            }
                            catch (PDOException $e) {
                                echo 'Erreur'.$e->getMessage();
                            }

                        ?>
                    </select>
                </div>

                <div class="container border-bottom border-danger">
                    <div class="col-4 my-3 offset-6 text-center">
                        <input class="mb-0" type="submit" name="submit" value="Enregistrer">
                    </div>
                </div>

        </form>
    </div>

