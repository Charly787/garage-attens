
    <?php include 'nav.php'; ?>

    <?php

        $nom           = isset($_POST['nom'])           && !empty($_POST['nom'])           ? $_POST['nom']           : '';
        $prenom        = isset($_POST['prenom'])        && !empty($_POST['prenom'])        ? $_POST['prenom']        : '';
        $adresse       = isset($_POST['adresse'])       && !empty($_POST['adresse'])       ? $_POST['adresse']       : '';
        $code_postal   = isset($_POST['code_postal'])   && !empty($_POST['code_postal'])   ? $_POST['code_postal']   : '';
        $ville         = isset($_POST['ville'])         && !empty($_POST['ville'])         ? $_POST['ville']         : '';
        $telephone     = isset($_POST['telephone'])     && !empty($_POST['telephone'])     ? $_POST['telephone']     : '';
        $date_embauche = isset($_POST['date_embauche']) && !empty($_POST['date_embauche']) ? $_POST['date_embauche'] : '';
        $identifiant   = isset($_POST['identifiant'])   && !empty($_POST['identifiant'])   ? $_POST['identifiant']   : '';
        $mot_de_passe  = isset($_POST['mot_de_passe'])  && !empty($_POST['mot_de_passe'])  ? $_POST['mot_de_passe']  : '';
        $statuts_admin = isset($_POST['statuts_admin']) && !empty($_POST['statuts_admin']) ? $_POST['statuts_admin'] : '';

        $submit        = isset($_POST['submit'])        && !empty($_POST['submit']) ? $_POST['submit'] : '';

        if ($submit) {
            try {
                $request = $pdo->prepare("INSERT INTO employe
                    (nom,prenom,adresse,code_postal,ville,telephone,date_embauche,identifiant,mot_de_passe,statuts_admin)
                    VALUES (:nom,:prenom,:adresse,:code_postal,:ville,:telephone,:date_embauche,:identifiant,:mot_de_passe,:statuts_admin)");
                $request->execute([
                    'nom'=>$nom,'prenom'=>$prenom,'adresse'=>$adresse,'code_postal'=>$code_postal,'ville'=>$ville,'telephone'=>$telephone,
                    'date_embauche'=>$date_embauche,'identifiant'=>$identifiant,'mot_de_passe'=>$mot_de_passe,'statuts_admin'=>$statuts_admin]);
                    header('Location: employe.php');

            }
            catch (PDOException $e) {
                echo 'Error: '.$e->getMessage();
            }
        }

    ?>

    <div class="container-fluid m-5 text-center">
        <div class="row">
            <div class="col-4 bg-dark text-white px-4 py-3">
                <h2>Nouveau personnel</h2>
            </div>
            <div class="col-1 mt-3 px-4">
                <a href="employe.php" class="btn btn-dark text-white p-2">Retour</a>
            </div>
        </div>
    </div>

    <form action="ajout-employe.php" method="post">
        <div class="container mt-5 p-0 d-flex font-weight-bold text-center">

            <div class="container border-bottom border-danger">
                <div class="col-10 offset-1">
                    Nom<br><input type="text" name="nom">
                </div>
                <div class="col-10 offset-1">
                    Prénom<br><input type="text" name="prenom">
                </div>
                <div class="col-10 offset-1">
                    Adresse<br><input type="text" name="adresse">
                </div>
                <div class="col-10 offset-1">
                    Code postal<br><input type="text" name="code_postal">
                </div>
                <div class="col-10 offset-1 border-bottom">
                    Ville<br><input type="text" name="ville">
                </div>
            </div>

            <div class="container border-bottom border-danger">
                <div class="col-10 offset-1">
                    Téléphone<br><input type="text" name="telephone" placeholder="00.00.00.00.00">
                </div>
                <div class="col-10 offset-1">
                    Date d'embauche <input type="date" name="date_embauche">
                </div>
                <div class="col-10 offset-1">
                    Identifiant <input type="text" name="identifiant">
                </div>
                <div class="col-10 offset-1">
                    Mot de passe <input type="password" name="mot_de_passe">
                </div>
                <div class="col-10 offset-1">
                    Statut Admin <input type="number" name="statuts_admin">
                </div>
            </div>

        </div>

        <div class="container border-bottom border-danger">
            <div class="col-4 my-3 offset-4 text-center">
                <input class="mb-0" type="submit" name="submit" value="Enregistrer">
            </div>
        </div>
        
    </form>


