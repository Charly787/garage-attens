
    <?php include 'nav.php'; ?>

    <?php

        $id_employe  = $_GET['id'];

        /* SELECTION DE TOUT LES EMPLOYES */
        try {

            $requete = $pdo->prepare("SELECT * FROM employe WHERE id_employe=?");
            $requete -> execute([$id_employe]);
            $requete = $requete -> fetchAll();
            $employe = $requete[0];

        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        // SUPPRESSION D'UN EMPLOYE
        $sup = isset($_GET['sup']) && !empty($_GET['sup']) ? $_GET['sup'] : '';

        if ($sup == 'ok') {
            if ($id_employe != null) {
                
                try {

                    $requete = $pdo->prepare("DELETE FROM employe WHERE id_employe=?");
                    $requete -> execute([$id_employe]);
                    header('Location: employe.php');

                }
                catch(PDOException $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            }
        }

    ?>

    <div class="container-fluid text-center">
        <div class="row m-5">
            <div class="col-4 bg-dark text-white px-4 py-3">
                <h2><?php echo $employe['prenom'].' '.$employe['nom']; ?></h2>
            </div>
            <div class="col-1 mt-3 px-4">
                <a href="employe.php" class="btn btn-dark text-danger p-2">Retour</a>
            </div>
            <div class="col-3 mt-4 offset-4">
                <?php echo '<a href="modifier-employe.php?id='.$employe['id_employe'].'" class="text-decoration-none bg-warning text-black mx-3 px-4 py-2">Modifier</a>' ?>
                <?php echo '<a href="?sup=ok&id='.$employe['id_employe'].'" class="text-decoration-none bg-danger text-black mx-3 px-4 py-2">Supprimer</a>' ?>
            </div>
        </div>
    </div>

    <div class="container d-flex font-weight-bold text-center">
        
        <div class="container mt-3">
            <div class="col-10">
                Adresse<div class="border-bottom font-weight-normal"><?php echo $employe['adresse']; ?></div><br>
            </div>
            <div class="col-10">
                Code postal<div class="border-bottom font-weight-normal"><?php echo $employe['code_postal']; ?></div><br>
            </div>
            <div class="col-10">
                Ville<div class="border-bottom font-weight-normal"><?php echo $employe['ville']; ?></div><br>
            </div>
            <div class="col-10">
                Téléphone<div class="border-bottom font-weight-normal"><?php echo $employe['telephone']; ?></div><br>
            </div>
        </div>

        <div class="container mt-3">
            <div class="col-10">
                Date d'embauche<div class="border-bottom font-weight-normal"><?php echo $employe['date_embauche']; ?></div><br>
            </div>
            <div class="col-10">
                Identifiant<div class="border-bottom font-weight-normal"><?php echo $employe['identifiant']; ?></div><br>
            </div>
            <div class="col-10">
                Mot de passe<div class="border-bottom font-weight-normal"><?php echo $employe['mot_de_passe']; ?></div><br>
            </div>
            <div class="col-10">
                Statut Admin<div class="border-bottom font-weight-normal"><?php echo $employe['statuts_admin']; ?></div><br>
            </div>
        </div>

    </div>

    <?php

    // AFFICHAGE DES INTERVENTIONS A VENIR D'UN EMPLOYÉ
        try {
            $request=$pdo->prepare('SELECT intitule,descrip,date,heure,duree,client FROM intervention
                                    WHERE mecanicien="'.$employe['prenom'].'"
                                    && date >= CURDATE() ORDER BY date');
            $request->execute([$id_employe]);

        }
        catch (PDOException $e) {
            echo 'Error: '.$e->getMessage();
        }

    ?>

    <div class="row intervention-employe mx-4 font-weight-bold">
        <div class="col-2">
            <div class="border-bottom border-danger">Intervention à venir :</div>
        </div>
    </div>

    <?php

        function inter ($request) {  ?>
            <div class="container-fluid text-center">
                
                <div class="row font-weight-bold">
                    <div class="col-1 offset-2 p-0 border-left border-danger">
                        <p class="border-bottom border-danger">Date</p>
                    </div>
                    <div class="col-1 p-0">
                        <p class="border-bottom border-danger">Intitulé</p>
                    </div>
                    <div class="col-4 p-0">
                        <p class="border-bottom border-danger">Description</p>
                    </div>
                    <div class="col-1 p-0">
                        <p class="border-bottom border-danger">Heure</p>
                    </div>
                    <div class="col-1 p-0">
                        <p class="border-bottom border-danger">Durée (mn)</p>
                    </div>
                    <div class="col-2 p-0">
                        <p class="border-bottom border-danger">Client</p>
                    </div>
                </div>

                <?php

                    while ($intervention = $request->fetch()) {

                    echo '
                            <div class="row">
                                <div class="col-1 pt-3 offset-2 border-bottom border-left border-danger">
                                    <p>'.$intervention['date'].'</p>
                                </div>
                                <div class="col-1 pt-3 border-bottom border-danger">
                                    <p>'.$intervention['intitule'].'</p>
                                </div>
                                <div class="col-4 pt-3 border-bottom border-danger">
                                    <p>'.$intervention['descrip'].'</p>
                                </div>
                                <div class="col-1 pt-3 border-bottom border-danger">
                                    <p>'.$intervention['heure'].'</p>
                                </div>
                                <div class="col-1 pt-3 border-bottom border-danger">
                                    <p>'.$intervention['duree'].'</p>
                                </div>
                                <div class="col-2 pt-3 border-bottom border-danger">
                                    <p>'.$intervention['client'].'</p>
                                </div>
                            </div>
                        ';/* return $intervention; */
                    }

                ?>

            </div>

    <?php } inter($request); ?>

    <?php

    // AFFICHAGE DES INTERVENTIONS EFFECTUÉES D'UN EMPLOYÉ
        try {
            $request=$pdo->prepare('SELECT intitule,descrip,date,heure,duree,client FROM intervention
                                    WHERE mecanicien="'.$employe['prenom'].'"
                                    && date <= CURDATE() ORDER BY date');
            $request->execute([$id_employe]);

        }
        catch (PDOException $e) {
            echo 'Error: '.$e->getMessage();
        }

    ?>

        <div class="row intervention-employe mx-4 mt-5 font-weight-bold">
            <div class="col-2">
                <div class="border-bottom border-danger">Intervention effectuées :</div>
            </div>
        </div>

    <?php inter($request); ?>
