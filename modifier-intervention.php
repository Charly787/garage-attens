
    <?php include 'nav.php'; ?>

    <?php

        $id_intervention = $_GET['id'];

        /* SELECTION DE TOUTES LES INTERVENTIONS */
        try {
            $requete = $pdo->prepare("SELECT * FROM intervention WHERE id_intervention=?");
            $requete -> execute([$id_intervention]);
            $requete = $requete -> fetchAll();
            $intervention = $requete[0];

        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    ?>

    <div class="container-fluid text-center">
        <div class="container-fluid text-center">
            <div class="row m-5">
                <div class="col-4 bg-dark text-white py-3">
                    <h2>intervention n° <?php echo $intervention['id_intervention']; ?></h2>
                </div>
                <div class="col-1 mt-3 px-4">
                    <a href="intervention.php" class="btn btn-dark text-danger p-2">Retour</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <form method="post">

            <div class="container mt-5 text-center font-weight-bold border-bottom border-danger">

                    <div class="col-10 offset-1">
                        Intitulé<br><input type="text" name="nom" value="<?php echo $intervention['intitule']; ?>">
                    </div>
                    <div class="col-10 offset-1">
                        Description<br><textarea name="descrip" cols="64" rows="3"><?php echo $intervention['descrip']; ?></textarea>
                    </div>
                    <div class="col-10 offset-1">
                        Date<br><input type="text" name="nom" value="<?php echo $intervention['date']; ?>">
                    </div>
                    <div class="col-10 offset-1">
                        Heure<br><input type="text" name="nom" value="<?php echo $intervention['heure']; ?>">
                    </div>
                    <div class="col-10 offset-1">
                        Client<br><input type="text" name="nom" value="<?php echo $intervention['client']; ?>">
                    </div>
                    <div class="col-10 offset-1">
                        Mécanicien<br><input type="text" name="nom" value="?">
                    </div>

            </div>

            <div class="container border-bottom border-danger">
                <div class="col-4 my-3 offset-4 text-center">
                    <input class="mb-0" type="submit" name="submit" value="Enregistrer">
                </div>
            </div>

        </form>

    </div>
